export type TeamPlacement = {
    name: string
    placement: number
}

export type GroupPrediction = {
    id: string
    teams: TeamPlacement[]
}