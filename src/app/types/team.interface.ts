export interface Team {
  name: string;
  imageUrl?: string;
}
