import { Component } from '@angular/core';
import {DISCORD_PAGES_URL, DISCORD_URL} from 'src/app/constants/env';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  DISCORD_URL = DISCORD_PAGES_URL;
}
