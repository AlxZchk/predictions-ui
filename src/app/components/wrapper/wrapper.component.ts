import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss']
})
export class WrapperComponent implements OnInit {
  showPrediction = false;
  hasCode = false;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    const snapshot = this.route.snapshot;
    const params = snapshot.queryParams;
    const { code } = params;

    if (code) {
      this.hasCode = true;
    }
  }
}
