import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {DiscordLoginService} from "../../services/discord-login/discord-login.service";

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.component.html',
  styleUrls: ['./after-login.component.scss']
})
export class AfterLoginComponent implements OnInit {
  @Output() showPredictions = new EventEmitter<void>();
  loading = true;

  constructor(
      private route: ActivatedRoute,
      private snackbar: MatSnackBar,
      private discordLoginService: DiscordLoginService
  ) {
  }

  ngOnInit() {
    const snapshot = this.route.snapshot;
    const params = snapshot.queryParams;
    const { code } = params;

    if (!code) {
      this.loading = false;
      this.snackbar.open('Failed to get authorization code. Try again', 'Close');
    }

    this.discordLoginService.fetchDiscordUserId(code)
        .subscribe(
            (userData) => {
                this.discordLoginService.setDiscordUser(userData);
                this.showPredictions.emit();
            },
            (error) => {
              console.warn(error);
              this.snackbar.open('Failed to get discord user id. Try again', 'Close');
              this.loading = false;
            }
        )
  }
}
