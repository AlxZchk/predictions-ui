import {Component, OnInit} from '@angular/core';
import {Team} from "../../types/team.interface";
import {TEAMS} from "../../constants/teams";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";
import {DiscordLoginService} from "../../services/discord-login/discord-login.service";
import {GroupPrediction, TeamPlacement} from "../../interfaces";

@Component({
  selector: 'app-predictions',
  templateUrl: './predictions.component.html',
  styleUrls: ['./predictions.component.scss']
})
export class PredictionsComponent implements OnInit {
  teams: Team[] = [...TEAMS];
  discordUser: any;

  constructor(
      private snackBar: MatSnackBar,
      private discordLoginService: DiscordLoginService
  ) {
  }

  ngOnInit() {
    this.discordUser = this.discordLoginService.getDiscordUser();
  }

  drop(event: CdkDragDrop<any>) {
    this.teams[event.previousContainer.data.index]=event.container.data.item;
    this.teams[event.container.data.index]=event.previousContainer.data.item;
    // console.log({ teams: this.teams });
  }

  moveUp(index: number) {
    if (index < 1) { return; }
    moveItemInArray(this.teams, index, index - 1);
    // console.log({ teams: this.teams });
  }

  moveDown(index: number) {
    if (index >= this.teams.length - 1) { return; }
    moveItemInArray(this.teams, index, index + 1);
    // console.log({ teams: this.teams });
  }

  save() {
    const { id } = this.discordUser ?? {};
    const placements: TeamPlacement[] = this.teams.map((team, idx) => ({ name: team.name, placement: idx + 1 }));
    const encodedId = encodeURI(id);

    const payload: GroupPrediction = { id: encodedId, teams: placements };

    return this.discordLoginService.sendPredictions(payload)
        .subscribe(
            (response) => this.snackBar.open("Predictions saved!", "Close"),
            (error) => {
              this.snackBar.open("Failed to save predictions.", "Close")
              console.warn(error);
            }
        )

  }

  trackByName<T extends { name: string | number }>(index: number, item: T) {
    return item.name;
  }
}
