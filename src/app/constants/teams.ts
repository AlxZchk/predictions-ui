import {Team} from "../types/team.interface";

export const TEAMS: Team[] = [
  { name: `h0ntr's Team`, imageUrl: 'h0ntr.png' },
  { name: `lockpick's Team`, imageUrl: 'lockpick.png' },
  { name: `MELOOO's Team`, imageUrl: 'melo.png' },
  { name: `Ispyz's Team`, imageUrl: 'ispyz.png' },
  { name: `Tamer's Team`, imageUrl: 'tamer.png' },
  { name: `Magik's Team`, imageUrl: 'magik.png' },
  { name: `dB's Team`, imageUrl: 'db.png' },
  { name: `subz's Team`, imageUrl: 'subz.png' },
  { name: `Hodor's Team`, imageUrl: 'hodor.png' },
  { name: `Gek's Team`, imageUrl: 'gek.png' },
  { name: `Sheeva's Team`, imageUrl: 'sheeva.png' },
  { name: `PuppetMastah's Team`, imageUrl: 'puppet.png' },
  { name: `chubbyrain's Team`, imageUrl: 'chubby.png' },
  { name: `Rocky's Team`, imageUrl: 'rocky.png' },
  { name: `maharaja's Team`, imageUrl: 'maharaja.png' },
  { name: `{DnF}MAG's Team`, imageUrl: 'mag.png' },
  { name: `Ch34t's Team`, imageUrl: 'ch34t.png' },
  { name: `Seven's Team`, imageUrl: 'seven.png' }
]
