import { Injectable } from '@angular/core';
import {
  DISCORD_CLIENT_ID,
  DISCORD_CLIENT_SECRET,
  DISCORD_PAGES_REDIRECT_URI,
  DISCORD_REDIRECT_URI
} from "../../constants/env";
import {HttpClient} from "@angular/common/http";
import {switchMap} from "rxjs";
import {GroupPrediction} from "../../interfaces";

@Injectable({
  providedIn: 'root'
})
export class DiscordLoginService {
  discordUser = null;

  constructor(private http: HttpClient) { }

  fetchDiscordUserId(code: string) {
    const params = new URLSearchParams({
      client_id: DISCORD_CLIENT_ID,
      client_secret: DISCORD_CLIENT_SECRET,
      grant_type: 'authorization_code',
      code,
      redirect_uri: DISCORD_PAGES_REDIRECT_URI
    })

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };

    return this.http.post('https://discord.com/api/oauth2/token', params, { headers })
        .pipe(
            switchMap((tokenResponse: any) => {
              return this.http.get('https://discord.com/api/users/@me', { headers: { Authorization: `Bearer ${tokenResponse.access_token }`, ...headers } })
            })
        )
  }

  setDiscordUser(user: any) {
    this.discordUser = user;
  }

  getDiscordUser() {
    return this.discordUser;
  }

  sendPredictions(prediction: GroupPrediction) {
    return this.http.post('https://utctfpug.com/predictions/send', prediction);
  }

  isUserSet() {
    return !!this.discordUser;
  }
}
